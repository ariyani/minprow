package com.example.med_id.med_id.repository;

import com.example.med_id.med_id.models.EducationLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EducationLevelRepo extends JpaRepository<EducationLevel, Long> {
}
