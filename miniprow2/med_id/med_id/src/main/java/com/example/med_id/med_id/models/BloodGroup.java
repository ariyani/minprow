package com.example.med_id.med_id.models;

import javax.persistence.*;

@Entity
@Table(name="m_blood_group")
public class BloodGroup extends CommonEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    //Column
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name = "code", nullable = true)
    private String Code;

    @Column(name = "description", nullable = true)
    private String Description;

    //Getter and Setter


    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
