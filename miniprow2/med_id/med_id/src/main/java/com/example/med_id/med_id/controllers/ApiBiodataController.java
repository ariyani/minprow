package com.example.med_id.med_id.controllers;


import com.example.med_id.med_id.models.Biodata;
import com.example.med_id.med_id.repository.BiodataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.ws.Response;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {

    @Autowired
    private BiodataRepo biodataRepo;

    @GetMapping(value = "indexBiodata")
    public ModelAndView indexBiodata() {
        ModelAndView view = new ModelAndView("biodata/indexBiodata");
        return view;
    }

    @GetMapping("/biodata")
    public ResponseEntity<List<Biodata>> GetAllBiodata() {
        try {
            List<Biodata> biodata = this.biodataRepo.findAll();
            return new ResponseEntity<>(biodata, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/biodata/{id}")
    public ResponseEntity<List<Biodata>> GetBiodataById(@PathVariable("id") Long id) {
        try {
            Optional<Biodata> biodata = this.biodataRepo.findById(id);

            if (biodata.isPresent()) {
                ResponseEntity responseEntity = new ResponseEntity<>(biodata,HttpStatus.OK);
                return responseEntity;
            } else
                return ResponseEntity.notFound().build();
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/biodata")
    public ResponseEntity<Object> SaveBiodata(@RequestBody Biodata biodata) {
        Biodata biodataData = this.biodataRepo.save(biodata);

        try {
            biodata.setCreatedBy(1);
            biodata.setCreatedOn(new Date());
            this.biodataRepo.save(biodata);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/biodata/{id}")
    public ResponseEntity<Object> UpdateBiodata(@RequestBody Biodata biodata,
                                                @PathVariable("id") Long id) {
        Optional<Biodata> doctorEducationData = this.biodataRepo.findById(id);

        if (doctorEducationData.isPresent()) {
            biodata.setId(id);
            biodata.setModifieBy(1);
            biodata.setModifieOn(new Date());
            this.biodataRepo.save(biodata);

            ResponseEntity responseEntity = new ResponseEntity<>("Success", HttpStatus.OK);
            return responseEntity;
        } else
            return ResponseEntity.notFound().build();
    }
}
//© 2021 GitHub, Inc.