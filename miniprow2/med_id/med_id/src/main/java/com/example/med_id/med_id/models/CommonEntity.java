package com.example.med_id.med_id.models;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
public class CommonEntity {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)

    @Column(name = "created_on", nullable = false)
    private Date createdOn;
    @Column (name ="created_by", nullable = false)
    private long createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)

    @Column(name = "modified_on", nullable = true)
    private Date modifieOn;
    @Column(name ="modified_by", nullable = true)
    private long modifieBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="delete_on", nullable = true)
    private Date deleteOn;

    @Column(name="delete_by", nullable = true)
    private long deleteBy;

    @Column(name = "isdelete", nullable = true)
    private Boolean isDelete = false;

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifieOn() {
        return modifieOn;
    }

    public void setModifieOn(Date modifieOn) {
        this.modifieOn = modifieOn;
    }

    public long getModifieBy() {
        return modifieBy;
    }

    public void setModifieBy(long modifieBy) {
        this.modifieBy = modifieBy;
    }

    public Date getDeleteOn() {
        return deleteOn;
    }

    public void setDeleteOn(Date deleteOn) {
        this.deleteOn = deleteOn;
    }

    public long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }
}
