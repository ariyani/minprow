package com.example.med_id.med_id.models;


import javax.persistence.*;
import java.security.PublicKey;

@Entity
@Table(name="m_doctor_education")
public class DoctorEducation extends CommonEntity{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    //Column
    @Column(name = "id", nullable = false)
    private long Id;

    @OneToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public Doctor doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long DoctorId;

    @ManyToOne
    @JoinColumn(name = "education_level_id", insertable = false, updatable = false)
    public EducationLevel educationLevel;

    @Column(name = "education_level_id", nullable = true)
    private Long EducationLevelId;

    @Column(name = "institution_name", length = 100, nullable = true)
    private String InstitutionName;

    @Column(name = "major", length = 100, nullable = true)
    private String Major;

    @Column(name = "start_year", length = 4, nullable = true)
    private String StartYear;

    @Column(name = "end_year", length = 4, nullable = true)
    private String EndYear;

    @Column(name = "is_las_education", nullable = true)
    private Boolean isLastEducation = false;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Long getDoctorId() {
        return DoctorId;
    }

    public void setDoctorId(Long doctorId) {
        DoctorId = doctorId;
    }

    public EducationLevel getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(EducationLevel educationLevel) {
        this.educationLevel = educationLevel;
    }

    public Long getEducationLevelId() {
        return EducationLevelId;
    }

    public void setEducationLevelId(Long educationLevelId) {
        EducationLevelId = educationLevelId;
    }

    public String getInstitutionName() {
        return InstitutionName;
    }

    public void setInstitutionName(String institutionName) {
        InstitutionName = institutionName;
    }

    public String getMajor() {
        return Major;
    }

    public void setMajor(String major) {
        Major = major;
    }

    public String getStartYear() {
        return StartYear;
    }

    public void setStartYear(String startYear) {
        StartYear = startYear;
    }

    public String getEndYear() {
        return EndYear;
    }

    public void setEndYear(String endYear) {
        EndYear = endYear;
    }

    public Boolean getLastEducation() {
        return isLastEducation;
    }

    public void setLastEducation(Boolean lastEducation) {
        isLastEducation = lastEducation;
    }
}
