package com.example.med_id.med_id.repository;

import com.example.med_id.med_id.models.Biodata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BiodataRepo extends JpaRepository<Biodata, Long> {
}
