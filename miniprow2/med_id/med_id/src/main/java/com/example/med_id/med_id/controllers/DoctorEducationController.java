package com.example.med_id.med_id.controllers;


import com.example.med_id.med_id.repository.DoctorEducationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "doctorEducation")
public class DoctorEducationController {


    @Autowired
    private DoctorEducationRepo doctorEducationRepo;

    @GetMapping(value="index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("doctorEducation/index");
        return view;
    }
}
