package com.example.med_id.med_id.repository;

import com.example.med_id.med_id.models.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepo extends JpaRepository<Doctor, Long> {
}
