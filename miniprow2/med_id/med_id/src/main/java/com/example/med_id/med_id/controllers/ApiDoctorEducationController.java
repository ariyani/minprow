package com.example.med_id.med_id.controllers;


import com.example.med_id.med_id.models.DoctorEducation;
import com.example.med_id.med_id.repository.DoctorEducationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiDoctorEducationController {

    @Autowired
    private DoctorEducationRepo doctorEducationRepo;

    @GetMapping("/doctorEducation")
    public ResponseEntity<List<DoctorEducation>> GetAllDoctorEducation(){
        try{
            List<DoctorEducation> doctorEducation = this.doctorEducationRepo.findAll();
            return new ResponseEntity<>(doctorEducation, HttpStatus.OK);
        } catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/doctorEducation")
    public ResponseEntity<Object> SaveDoctorEducation(@RequestBody DoctorEducation doctorEducation) {
        DoctorEducation doctorEducationData = this.doctorEducationRepo.save(doctorEducation);
        try {
            doctorEducation.setCreatedOn(new Date());
            doctorEducation.setCreatedBy(123456);
            this.doctorEducationRepo.save(doctorEducation);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("doctorEducationmapped")
    public ResponseEntity<Map<String, Object>> GetAllpage(@RequestParam(defaultValue = "0")int page,
                                                          @RequestParam(defaultValue = "5")int size)
    {
        try {
            List<DoctorEducation> doctorEducation = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<DoctorEducation> pageTuts;
            pageTuts = doctorEducationRepo.findAll(pagingSort);
            doctorEducation = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("doctorEducation", doctorEducation);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/doctorEducation/{id}")
    public ResponseEntity<Object> UpdateDoctorEducation(@RequestBody DoctorEducation doctorEducation, @PathVariable("id") Long id) {
        Optional<DoctorEducation> doctorEducationData = this.doctorEducationRepo.findById(id);
        if (doctorEducationData.isPresent()) {
            doctorEducation.setId(id);
            doctorEducation.setModifieOn(new Date());
            doctorEducation.setModifieBy(123456);

            this.doctorEducationRepo.save(doctorEducation);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }


}
