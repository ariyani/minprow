package com.example.med_id.med_id.controllers;


import com.example.med_id.med_id.models.Customer;
import com.example.med_id.med_id.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping ("/api")
public class ApiCustomerController {
    @Autowired
    private CustomerRepo customerRepo;

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> GetAllCustomer(){
        try
        {
            List<Customer> customer = this.customerRepo.findAll();
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/customer")
    public ResponseEntity<Object> SaveCustomer(@RequestBody Customer customer) {
        Customer customerData = this.customerRepo.save(customer);
        try {
            customer.setCreatedOn(new Date());
            customer.setCreatedBy(123456);
            this.customerRepo.save(customer);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<List<Customer>> GetCustomerById(@PathVariable("id") Long id) {
        try {
            Optional<Customer> customer = this.customerRepo.findById(id);
            if (customer.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(customer, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
