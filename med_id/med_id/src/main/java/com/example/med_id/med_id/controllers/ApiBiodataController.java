package com.example.med_id.med_id.controllers;


import com.example.med_id.med_id.models.Biodata;
import com.example.med_id.med_id.repository.BiodataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.ws.Response;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {

    @Autowired
    private BiodataRepo biodataRepo;

    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("biodata/index");
        return view;
    }


    @GetMapping("/biodata")
    public ResponseEntity<List<Biodata>> GetAllBiodata() {
        try {
            List<Biodata> biodata = this.biodataRepo.findAll();
            return new ResponseEntity<>(biodata, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/biodata")
    public ResponseEntity<Object> SaveBiodata(@RequestBody Biodata biodata){
        Biodata biodataData = this.biodataRepo.save(biodata);
        try
        {
            biodata.setCreatedOn(new Date());
            biodata.setCreatedBy(123456);
            this.biodataRepo.save(biodata);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch
        (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/biodata/{id}")
    public ResponseEntity<List<Biodata>> GetBiodataById(@PathVariable("id") Long id){
        try {
            Optional<Biodata> biodata = this.biodataRepo.findById(id);
            if (biodata.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("biodatamapped")
    public ResponseEntity<Map<String, Object>> GetAllPage (@RequestParam(defaultValue = "0")int page,
                                                           @RequestParam(defaultValue = "5")int size)
    {
        try{
            List<Biodata> biodata = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Biodata> pageTuts;
            pageTuts = biodataRepo.findAll(pagingSort);
            biodata = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("biodata", biodata);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/biodata/{id}")
    public ResponseEntity<Object> UpdateBiodata(@RequestBody Biodata biodata, @PathVariable("id") Long id){
        Optional<Biodata> biodataData = this.biodataRepo.findById(id);
        if(biodataData.isPresent()) {
            biodata.setId(id);
            biodata.setModifieOn(new Date());
            biodata.setModifieBy(123456);

            this.biodataRepo.save(biodata);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }
}
