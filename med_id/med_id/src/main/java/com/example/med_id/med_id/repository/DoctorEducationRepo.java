package com.example.med_id.med_id.repository;

import com.example.med_id.med_id.models.DoctorEducation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorEducationRepo extends JpaRepository<DoctorEducation, Long> {
}
