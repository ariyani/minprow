package com.example.med_id.med_id.repository;

import com.example.med_id.med_id.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
}
