package com.example.med_id.med_id.repository;

import com.example.med_id.med_id.models.BloodGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BloodGroupRepo extends JpaRepository<BloodGroup, Long> {
}
