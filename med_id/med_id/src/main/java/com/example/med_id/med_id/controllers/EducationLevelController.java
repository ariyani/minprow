package com.example.med_id.med_id.controllers;


import com.example.med_id.med_id.models.EducationLevel;
import com.example.med_id.med_id.repository.EducationLevelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping (value = "/educationLevel")
public class EducationLevelController {
    @Autowired
    private EducationLevelRepo educationLevelRepo;

    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("educationLevel/index");
        return view;
    }
}
