package com.example.med_id.med_id.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "m_customer")
public class Customer extends CommonEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    //COLUMN
    @Column(name = "id", nullable = false)
    private long Id;

    @OneToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata;

    @Column(name = "biodata_id", nullable = true)
    private long BiodataId;

    @Column(name="dob", nullable = true)
    private Date Dob;

    @Column(name="gender", nullable = true)
    private String Gender;

    @Column(name="blood_group_id", nullable = true)
    private long BloodGroupId;

    @Column(name="rhesus_type", nullable = true)
    private String RhesusType;

    @Column(name="height", nullable = true)
    private Double Height;

    @Column(name="weight", nullable = true)
    private Double Weight;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public long getBiodataId() {
        return BiodataId;
    }

    public void setBiodataId(long biodataId) {
        BiodataId = biodataId;
    }

    public Date getDob() {
        return Dob;
    }

    public void setDob(Date dob) {
        Dob = dob;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public long getBloodGroupId() {
        return BloodGroupId;
    }

    public void setBloodGroupId(long bloodGroupId) {
        BloodGroupId = bloodGroupId;
    }

    public String getRhesusType() {
        return RhesusType;
    }

    public void setRhesusType(String rhesusType) {
        RhesusType = rhesusType;
    }

    public Double getHeight() {
        return Height;
    }

    public void setHeight(Double height) {
        Height = height;
    }

    public Double getWeight() {
        return Weight;
    }

    public void setWeight(Double weight) {
        Weight = weight;
    }
}
